import cv2
import os

import matplotlib

# Read in the cascade classifiers
charlie_cascade = cv2.CascadeClassifier('data/cascade.xml')
   
def detect_charlie(img):
     
    charlie_img = img.copy()
    charlie_rect = charlie_cascade.detectMultiScale(charlie_img)
    print(charlie_rect)
    for (x, y, w, h) in charlie_rect:
        cv2.rectangle(charlie_img, (x, y),
                      (x + w, y + h), (0, 0, 255), 2)       
    return charlie_img
 

directory = "undefined/"
for filename in os.listdir(directory):
    f_read = os.path.join(directory, filename)
    f_write = os.path.join("detected_charlie/", filename)
    img = cv2.imread(f_read)
    face = detect_charlie(img)
    #matplotlib.plt.imshow(face)
    print(len(face))
    cv2.imwrite(f_write, face)